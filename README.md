# rails-tutorial-docker

Ruby on Rails チュートリアル のための仮想環境構築リポジトリです。

## 事前準備

以下がインストールされていること  

* Docker for Windows v2.0.0.3
* docker-compose v1.24.0

## 環境構築

以下のディレクトリ構成を前提とします。
```
rails-tutorial
 ├ rails-tutorial-docker ← 本リポジトリ
 ├ hello_app ← rails アプリケーション
 ├ toy_app
 └ sample_app
```

### rails の環境構築

1. `.env.examle` を参考に `.env` ファイルを `rails-tutorial-docker` 直下に作成します。  
rails アプリケーション未作成の場合は `APP_ROOT` の値を `rails-tutorial-docker` にしてください。  
rails アプリケーション作成済みの場合は `APP_ROOT` の値をアプリケーションがあるフォルダ名にしてください。
1. 以下のコマンドを実行します。

        #!shell

        $ docker-compose run --rm app bundle install

1. rails アプリケーションを作成します。（作成済みの場合はスキップ）  
hello_app を作成する例です。

        #!shell

        $ docker-compose run --rm app rails _5.1.6_ new hello_app
        $ mv hello_app ..

1. `.env` の `APP_ROOT` を `hello_app` に書き換えます。（作成済みの場合はスキップ）
1. rails アプリケーションを起動します。

        #!shell

        $ docker-compose up --build -d（初回起動時）
        $ docker-compose up -d（2回目以降の起動）

1. http://localhost:3000 にアクセスし、画面が表示されれば成功です。

### heroku コマンドを使用するための準備

1. heroku に登録する SSH鍵を `./docker/heroku/.ssh` に配置してください。

#### 各コマンド

* ログイン

        #!shell

        $ docker-compose run --rm heroku heroku login --interactive

* heroku にSSH鍵登録

        #!shell

        $ docker-compose run --rm heroku heroku keys:add

* heroku に新しいアプリケーションを作成

        #!shell

        $ docker-compose run --rm heroku heroku create

* heroku にデプロイ

        #!shell

        $ docker-compose run --rm heroku git push heroku master

## docker コマンド

* 起動中のコンテナを確認する

        #!shell

        $ docker-compose ps

* コンテナを起動する（`--build` は初回起動時のみで良い）

        #!shell

        $ docker-compose up --build -d

* docker を実行する（実行したコンテナを削除しない場合は `--rm` を外す）

        #!shell

        $ docker-compose run --rm サービス名 コマンド

* 起動中のコンテナに入る（イメージが `alpine` の場合は `ash`, それ以外は `bash`）

        #!shell

        $ docker-compose exec サービス名 ash

* コンテナを終了する

        #!shell

        $ docker-compose down

* コンテナを停止する

        #!shell

        // すべて停止
        $ docker-compose stop
        // 個別に停止
        $ docker-compose stop サービス名

* 停止しているコンテナを再開する

        #!shell

        // すべて再開
        $ docker-compose start
        // 個別に再開
        $ docker-compose start サービス名

* コンテナを再起動する (stop + start の合わせ技)

        #!shell

        // すべて再起動
        $ docker-compose restart
        // 個別に再起動
        $ docker-compose restart サービス名

* コンテナのログを表示する

        #!shell

        $ docker-compose logs -f サービス名

## トラブルシューティング

Q. コンテナにホストディレクトリがマウントされない  
A. windows の場合は相対パスが使えないので `/c/` から始まる絶対パスで指定してみてください。

Q. bundle install してもインストールされない or 更新されない  
A. docker volume にデータが保存されているので一旦削除してから再作成してみてください。
```
#!shell

$ docker-compose down
$ docker volume rm rails-tutorial-docker_bundle-data
$ docker-compose run --rm app bundle install
$ docker-compose up -d
```

Q. `docker-compose run --rm heroku heroku run rails db:migrate` すると `ECONNREFUSED: connect ECONNREFUSED 50.19.103.36:5000` というエラーになってしまう  
A. `docker-compose run --rm heroku heroku run:detached rails db:migrate` を試してみてください。

Q. debugger の byebug プロンプトを確認するには？  
A. `docker attach rails-tutorial-docker_app_1`  
　 終了するときは `ctrl+c`
